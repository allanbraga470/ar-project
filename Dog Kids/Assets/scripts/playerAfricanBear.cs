﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerAfricanBear : MonoBehaviour {

	public Animation animacao;
	private int n;
	public AudioClip bearAttack;
	public AudioClip bearDamage;
	public AudioClip bearIdle;
	public AudioClip bearRoar;

	// Use this for initialization
	void Start () { 
		n = 0;
		animacao = this.GetComponent<Animation> ();
	}
	
	// Update is called once per frame
	void OnMouseDown(){
		if (Input.GetMouseButtonDown (0)) {
			switch (n) {
			case 0:
				animacao.Play ("attack");
				GetComponent<AudioSource> ().PlayOneShot (bearAttack,0.7F);
				n++;
				break;
			case 1:
				animacao.Play ("death");
				n++;
				break;
			case 2:
				animacao.Play ("eat");
				n++;
				break;
			case 3:
				animacao.Play ("hurt");
				GetComponent<AudioSource> ().PlayOneShot (bearDamage,0.7F);
				n++;
				break;
			case 4:
				animacao.Play ("idle");
				GetComponent<AudioSource> ().PlayOneShot (bearIdle,0.7F);
				n++;
				break;
			case 5:
				animacao.Play ("idle2");
				GetComponent<AudioSource> ().PlayOneShot (bearIdle,0.7F);
				n++;
				break;
			case 6:
				animacao.Play ("roar");
				GetComponent<AudioSource> ().PlayOneShot (bearRoar,0.7F);
				n++;
				break;
			}
			if (n == 7)
				n = 0;
		}
	}
}
