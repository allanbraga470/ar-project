﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class introduction : MonoBehaviour {

	int firstRun = 0;

	// Use this for initialization
	void Start () {
		firstRun = PlayerPrefs.GetInt ("savedFirstRun");
		firstRun++;
		PlayerPrefs.SetInt ("savedFirstRun", firstRun);
	}
}
