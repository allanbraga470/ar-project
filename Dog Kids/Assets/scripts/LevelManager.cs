﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {

	int firstRun = 0;
	public Texture2D LoadScreenTexture;
	AsyncOperation loading = null;
	public Transform canvas;

	public Button buttonOption1;
	public Button buttonOption2;
	public Button buttonOption3;
	public Button buttonCancel;
	public Text sum;
	public Text option_1;
	public Text option_2;
	public Text option_3;


	private int rightNumber;
	private int option1 = 0;
	private int option2 = 0;
	private int option3 = 0;
	private int lastOption = 0;

	// Use this for initialization
	void Start () {
		firstRun = PlayerPrefs.GetInt ("savedFirstRun");
		if (firstRun <= 2) {
			PlayerPrefs.SetInt ("savedFirstRun", firstRun);
			Application.LoadLevel ("introduction");
		}
	}
		
	 
	public void loadScene(string name){	 
		//Application.LoadLevel (name);
		loading = Application.LoadLevelAsync(name); 
	}

	void OnGUI()
	{
		if (Application.isLoadingLevel)
			GUI.DrawTexture(new Rect(0, -50, Screen.width, Screen.height), LoadScreenTexture);
	}

	public void openUrl(){
		Application.OpenURL ("http://brinquedosfoods.com.br/realidade-aumentada/");
		canvas.gameObject.SetActive (false);
		Time.timeScale = 1;
	}


	public void openParentalView(){
		generateParentalValues ();
		if (canvas.gameObject.activeInHierarchy == false) {
			canvas.gameObject.SetActive (true);
			Time.timeScale = 0;
		} else {
			canvas.gameObject.SetActive (false);
			Time.timeScale = 1;
		}
	}


	public void generateParentalValues(){
		int randomNumber = Random.Range(1,30);
		int randomNumber2 = Random.Range(1,30);
		int rightNumber = randomNumber + randomNumber2;
		option1 = 0;
		option2 = 0;
		option3 = 0;


		int randomOption;
		do {
			randomOption = Random.Range (1, 3);
		} while(randomOption == lastOption);

		switch (randomOption) {
		case 1:
			option_1.text = "" + rightNumber;
			option1 = rightNumber;
			break;
		case 2:
			option_2.text = ""+rightNumber;
			option2 = rightNumber;
			break;
		case 3:
			option_3.text = ""+rightNumber;
			option3 = rightNumber;
			break;
		}

		if (option1 == 0) {
			do {
				option1 = Random.Range (1, 30);
			} while(option1 == rightNumber);
		}
		if (option2 == 0) {
			do {
				option2 = Random.Range (1, 30);
			} while(option2 == rightNumber || option2 == option1);
		}
		if (option3 == 0) {
			do {
				option3 = Random.Range (1, 30);
			} while(option3 == rightNumber || option3 == option1 || option3 == option2);
		}

		sum.text = randomNumber + " + " + randomNumber2;
		option_1.text = ""+option1;
		option_2.text = ""+option2;
		option_3.text = ""+option3;

		this.rightNumber = rightNumber;
	}

	public void quitGame(){
		Application.Quit();
	}

	public void optionSelectedParental(int option){
		switch (option) {
		case 1:
			lastOption = option;
			if (option1 == rightNumber) {
				openUrl ();
			} else {
				generateParentalValues ();
			}
			break;
		case 2:
			lastOption = option;
			if(option2 == rightNumber){
				openUrl ();
			}else {
				generateParentalValues ();
			}
			break;
		case 3:
			lastOption = option;
			if(option3 == rightNumber){
				openUrl ();
			}else {
				generateParentalValues ();
			}
			break;
		case 4:
			canvas.gameObject.SetActive (false);
			Time.timeScale = 1;
			break;
		}
	}
}
