﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerAfricanBoar : MonoBehaviour {

	public Animation animacao;
	private int n;
	public AudioClip boarDamage;
	public AudioClip boarDeath;
	public AudioClip boarIdle;

	// Use this for initialization
	void Start () { 
		n = 0;
		animacao = this.GetComponent<Animation> ();

	}

	void Update(){
		if(!animacao.isPlaying)
			animacao.Play ("idle");
	}
	
	// Update is called once per frame
	void OnMouseDown(){
		if (Input.GetMouseButtonDown (0)) {
			switch (n) {
			case 0:
				animacao.Play ("death");
				GetComponent<AudioSource> ().PlayOneShot (boarDeath,0.7F);
				n++;
				break;
			case 1:
				animacao.Play ("eats");
				n++;
				break;
			case 2:
				animacao.Play ("hurt");
				GetComponent<AudioSource> ().PlayOneShot (boarDamage,0.7F);
				n++;
				break;
			case 3:
				animacao.Play ("idle");
				GetComponent<AudioSource> ().PlayOneShot (boarIdle,0.7F);
				n++;
				break;
			}
			if (n == 4)
				n = 0;
		}
	}
}
