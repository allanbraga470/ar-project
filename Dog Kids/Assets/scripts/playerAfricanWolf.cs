﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerAfricanWolf : MonoBehaviour {

	public Animation animacao;
	private int n;
	public AudioClip wolfBark;
	public AudioClip wolfDamage;
	public AudioClip wolfHowl;
	public AudioClip wolfIdle;
	public AudioClip wolfRoar;

	// Use this for initialization
	void Start () { 
		n = 0;
		animacao = this.GetComponent<Animation> ();
	}
	
	// Update is called once per frame
	void OnMouseDown(){
		if (Input.GetMouseButtonDown (0)) {
			switch (n) {
			case 0:
				animacao.Play ("attack");
				GetComponent<AudioSource> ().PlayOneShot (wolfRoar,0.7F);
				n++;
				break;
			case 1:
				animacao.Play ("attack2");
				GetComponent<AudioSource> ().PlayOneShot (wolfDamage,0.7F);
				n++;
				break;
			case 2:
				animacao.Play ("death");
				n++;
				break;
			case 3:
				animacao.Play ("eat");
				n++;
				break;
			case 4:
				animacao.Play ("howl");
				GetComponent<AudioSource> ().PlayOneShot (wolfHowl,0.7F);
				n++;
				break;
			case 5:
				animacao.Play ("hurt");
				GetComponent<AudioSource> ().PlayOneShot (wolfBark,0.7F);
				n++;
				break;
			case 6:
				animacao.Play ("idle");
 
				n++;
				break;
			case 7:
				animacao.Play ("idle2");
 
				n++;
				break;
			}
			if (n == 8)
				n = 0;
		}
	}
}
