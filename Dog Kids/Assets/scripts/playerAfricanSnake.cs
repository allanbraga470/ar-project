﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerAfricanSnake : MonoBehaviour {

	public Animation animacao;
	private int n;
	public AudioClip snakeIdle;


	// Use this for initialization
	void Start () { 
		n = 0;
		animacao = this.GetComponent<Animation> ();

	}
	
	// Update is called once per frame
	void OnMouseDown(){
		if (Input.GetMouseButtonDown (0)) {
			switch (n) {
			case 0:
				animacao.Play ("attack");
				n++;
				break;
			case 1:
				animacao.Play ("death");
				n++;
				break;
			case 2:
				animacao.Play ("hurt");
				n++;
				break;
			case 3:
				animacao.Play ("idle");
				GetComponent<AudioSource> ().PlayOneShot (snakeIdle,0.7F);
				n++;
				break;
		
			}
			if (n == 4)
				n = 0;
		}
	}


	void Update(){
		if(!animacao.isPlaying)
			animacao.Play ("idle");
	}
}
