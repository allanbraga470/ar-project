﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour {

	public Animator anim;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			int n = Random.Range (0,4);
			switch (n) {
			case 0:
				anim.Play ("WAIT01",-1,0f);
				break;
			case 1:
				anim.Play ("WAIT02",-1,0f);
				break;
			case 2:
				anim.Play ("WAIT03",-1,0f);
				break;
			case 3:
				anim.Play ("WAIT04",-1,0f);
				break;
			}
		}
	}
}
