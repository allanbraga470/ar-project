﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerCat : MonoBehaviour {


	public AudioClip soundMeow;
	public AudioClip soundJump;
	public Animator anim;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> (); 
	}
		
	void OnMouseDown(){
			int n = Random.Range (0, 5);
			switch (n) {
			case 0:
				anim.Play ("Idle", -1, 0f);
				break;
			case 1:
				anim.Play ("Walk", -1, 0f);
				break;
			case 2:
				anim.Play ("sound", -1, 0f);
				GetComponent<AudioSource> ().PlayOneShot (soundMeow,0.7F);
				break;
			case 3:
				anim.Play ("Jump", -1, 0f);
			GetComponent<AudioSource> ().PlayOneShot (soundJump,0.7F);
				break;
			case 4:
				anim.Play ("Eat", -1, 0f);
				break;
			 
		}
	}
}
