﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerTriceratops : MonoBehaviour {

	public Animation animacao;
	private int n;
	public AudioClip triceratopsAttack;
	public AudioClip triceratopsHurt;
	public AudioClip triceratopsDeath;

	// Use this for initialization
	void Start () { 
		n = 0;
		animacao = this.GetComponent<Animation> ();

	}

	void Update(){
		if(!animacao.isPlaying)
			animacao.Play ("idle");
	}

	// Update is called once per frame
	void OnMouseDown(){
		if (Input.GetMouseButtonDown (0)) {
			switch (n) {
			case 0:
				animacao.Play ("attack");
				GetComponent<AudioSource> ().PlayOneShot (triceratopsAttack,0.7F);
				n++;
				break;
			case 1:
				animacao.Play ("hurt");
				GetComponent<AudioSource> ().PlayOneShot (triceratopsHurt,0.7F);
				n++;
				break;
			case 2:
				animacao.Play ("eat");
				n++;
				break;
			case 3:
				animacao.Play ("idle");
				n++; 
				break;
			case 4:
				animacao.Play ("death");
				GetComponent<AudioSource> ().PlayOneShot (triceratopsDeath,0.7F);
				n++;
				break;
			case 5:
				animacao.Play ("idle");
				n++; 
				break;
			}
			if (n == 6)
				n = 0;
		}
	}
}
