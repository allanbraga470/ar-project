﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerIguana : MonoBehaviour {

	public Animator anim;
	public AudioClip somHit;
	public AudioClip somAttack;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> (); 
	}
		
	void OnMouseDown(){
			int n = Random.Range (0, 3);
			print (n);

			switch (n) {
			case 0:
				anim.Play ("Idle", -1, 0f);
				break;
			case 1:
				anim.Play ("Hit", -1, 0f);
				GetComponent<AudioSource> ().PlayOneShot (somHit,0.7F);
				break;
			case 2:
				anim.Play ("Attack", -1, 0f);
				GetComponent<AudioSource> ().PlayOneShot (somAttack,0.7F);
				break;
		 
		}
	}
}
