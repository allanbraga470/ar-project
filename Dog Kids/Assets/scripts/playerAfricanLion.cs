﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerAfricanLion : MonoBehaviour {

	public Animation animacao;
	private int n;
	public AudioClip somAttack;
	public AudioClip somCall;
	public AudioClip somDeath;

	// Use this for initialization
	void Start () { 
		n = 0;
		animacao = this.GetComponent<Animation> ();
	}
	
	// Update is called once per frame
	void OnMouseDown(){
		if (Input.GetMouseButtonDown (0)) {
			switch (n) {
			case 0:
				animacao.Play ("hurt");
				GetComponent<AudioSource> ().PlayOneShot (somCall,0.7F);
				n++;
				break;
			case 1:
				animacao.Play ("attack");
				GetComponent<AudioSource> ().PlayOneShot (somAttack,0.7F);
				n++;
				break;
			case 2:
				animacao.Play ("death");
				GetComponent<AudioSource> ().PlayOneShot (somDeath,0.7F);
				n++;
				break;
			case 3:
				animacao.Play ("idle");
				n++;
				break;
			case 4:
				animacao.Play ("eat");
				n++;
				break;
			}
			if (n == 5)
				n = 0;
		}
	}
}
