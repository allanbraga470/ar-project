﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerTiger : MonoBehaviour {

	public Animator anim;
	public AudioClip somRuge;
	public AudioClip somHit;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> (); 
	}
		
	void OnMouseDown(){
			int n = Random.Range (0, 3);
			switch (n) {
			case 0:
				anim.Play ("idle", -1, 0f);
				break;
			case 1:
				anim.Play ("hit", -1, 0f);
				GetComponent<AudioSource> ().PlayOneShot (somHit,0.7F);
				break;
			case 2:
				anim.Play ("sound", -1, 0f);
				GetComponent<AudioSource> ().PlayOneShot (somRuge,0.7F);
				break;
		 
		}
	}
}
