﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class movePersonagem : MonoBehaviour {

	public GameObject jogador;
	public Animation animacao;

	public GameObject particulaOvo;
	public GameObject particulaPena;
	public GameObject particulaEstrela;
	public GameObject particulaFogo;

	CharacterController objetoCharController;
	float velocidade = 5.0f;
	float giro = 3.0f;
	float frente = 3.0f;
	private float pulo = 5.0f; 
	Vector3 vetorDirecao = new Vector3(0,0,0);
	Vector3 moveCameraFrente;
	Vector3 moveMove;
	Vector3 normalZeroPiso = new Vector3(0,0,0);
	Transform transformCamera;

	float contaPisca;
	bool podePegarStar;
	int numberObjects;


	public AudioClip somOvo;
	public AudioClip somPena;
	public AudioClip somEstrela;
	public AudioClip somHint;
	public AudioClip somWin;
	public AudioClip somLose;
	public AudioClip somApareceStar;
	public AudioClip somFelupudoVoa;

	void Start () {
		objetoCharController = GetComponent<CharacterController>();
		animacao = jogador.GetComponent<Animation> ();
		transformCamera = Camera.main.transform;
	}
	

	void Update(){
		moveCameraFrente = Vector3.Scale (transformCamera.forward, new Vector3(1,0,1)).normalized;
		moveMove = CrossPlatformInputManager.GetAxis ("Vertical") * moveCameraFrente + CrossPlatformInputManager.GetAxis("Horizontal") * transformCamera .right;

		vetorDirecao.y -= 3.0f * Time.deltaTime;
		objetoCharController.Move (vetorDirecao * Time.deltaTime);
		objetoCharController.Move (moveMove * velocidade * Time.deltaTime);

		if (moveMove.magnitude > 1f) moveMove.Normalize ();
		moveMove = transform.InverseTransformDirection (moveMove);

		moveMove = Vector3.ProjectOnPlane (moveMove, normalZeroPiso);
		giro = Mathf.Atan2 (moveMove.x,moveMove.z);
		frente = moveMove.z;

		objetoCharController.SimpleMove (Physics.gravity);
		aplicaRotacao ();

		if (CrossPlatformInputManager.GetButton ("Jump")) {
			if (objetoCharController.isGrounded == true)
				vetorDirecao.y = pulo;
			jogador.GetComponent<Animation> ().Play ("JUMP");
			GetComponent<AudioSource> ().PlayOneShot (somFelupudoVoa,0.7F);
		} else {
			if ((CrossPlatformInputManager.GetAxis ("Horizontal") != 0.0f) || (CrossPlatformInputManager.GetAxis ("Vertical") != 0.0f)) {
				if (!animacao.IsPlaying ("JUMP")) {
					jogador.GetComponent<Animation> ().Play ("WALK");
				}
			} else {
				if (objetoCharController.isGrounded == true) {
					jogador.GetComponent<Animation> ().Play ("IDLE");
				}
			}
		}
	}

	void aplicaRotacao(){
		float turnSpeed = Mathf.Lerp (180, 360, frente);
		transform.Rotate (0,giro * turnSpeed * Time.deltaTime,0);
	}

	void OnTriggerEnter(Collider other){
		if (other.gameObject.tag == "OVO") {
			Instantiate (particulaOvo, other.gameObject.transform.position, Quaternion.identity);
			other.gameObject.SetActive (false);
			numberObjects++; verificaPickObject ();
			GetComponent<AudioSource> ().PlayOneShot (somOvo,0.7F);
		}
		if (other.gameObject.tag == "PENA") {
			Instantiate (particulaPena, other.gameObject.transform.position, Quaternion.identity);
			other.gameObject.SetActive (false);
			numberObjects++; verificaPickObject ();
			GetComponent<AudioSource> ().PlayOneShot (somPena,0.7F);
		}
		if (other.gameObject.tag == "ESTRELA") {
			if (podePegarStar) {
				Instantiate (particulaEstrela, other.gameObject.transform.position, Quaternion.identity);
				other.gameObject.SetActive (false);
				Invoke ("carregaFase",3);
				GetComponent<AudioSource> ().PlayOneShot (somEstrela,0.7F);
				GetComponent<AudioSource> ().PlayOneShot (somWin,0.7F);
			}
		}
		if (other.gameObject.tag == "FOGUEIRA") {
			InvokeRepeating ("mudaEstadoFelpudo",0,0.1f);
			objetoCharController.Move (transform.TransformDirection(Vector3.back)*3);
			GetComponent<AudioSource> ().PlayOneShot (somHint,0.7F);
		}
		if (other.gameObject.tag == "BURACO") {
			Invoke ("carregaFase",2);
			GetComponent<AudioSource> ().PlayOneShot (somLose,0.7F);
		}
	}

	void mudaEstadoFelpudo(){
		contaPisca++;
		jogador.SetActive (!jogador.activeInHierarchy);
		if (contaPisca > 7) {
			contaPisca = 0;
			jogador.SetActive (true);
			CancelInvoke ();
		}
	}

	void verificaPickObject(){
		if (numberObjects > 19) {
			podePegarStar = true;
			Destroy (particulaFogo);
			GetComponent<AudioSource> ().PlayOneShot (somApareceStar,0.7F);
		}
	}

	void carregaFase(){
		Application.LoadLevel ("CenaAventura");
	}

	/**
	void Update () {
		Vector3 forward = Input.GetAxis ("Vertical") * transform.TransformDirection (Vector3.forward) * velocidade;
		transform.Rotate (new Vector3(0,Input.GetAxis("Horizontal") * giro * Time.deltaTime,0));

		objetoCharController.Move (forward * Time.deltaTime);
		objetoCharController.SimpleMove (Physics.gravity);
		 
		if (Input.GetButton ("Jump")) {
			if (objetoCharController.isGrounded == true)
				vetorDirecao.y = pulo;
				jogador.GetComponent<Animation> ().Play ("JUMP");
		} else {
			if (Input.GetButton ("Horizontal") || Input.GetButton ("Vertical")) {
				if (!animacao.IsPlaying ("JUMP")) {
					jogador.GetComponent<Animation> ().Play ("WALK");
				}
			} else {
				if (objetoCharController.isGrounded == true) {
					jogador.GetComponent<Animation> ().Play ("IDLE");
				}
			}
		}

		vetorDirecao.y -= 3.0f * Time.deltaTime;
		objetoCharController.Move (vetorDirecao * Time.deltaTime);
	}
**/
}
