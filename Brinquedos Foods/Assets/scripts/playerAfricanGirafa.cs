﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerAfricanGirafa : MonoBehaviour {

	public Animation animacao;
	private int n;
	public AudioClip somHurt;
	public AudioClip somDeath;
	// Use this for initialization
	void Start () { 
		n = 0;
		animacao = this.GetComponent<Animation> ();
	}
	
	// Update is called once per frame
	void OnMouseDown(){
		if (Input.GetMouseButtonDown (0)) {
			switch (n) {
			case 0:
				animacao.Play ("hurt");
				GetComponent<AudioSource> ().PlayOneShot (somHurt,0.7F);
				n++;
				break;			
			case 1:
				animacao.Play ("death");
				GetComponent<AudioSource> ().PlayOneShot (somDeath,0.7F);
				n++;
				break;
			case 2:
				animacao.Play ("idle");
				n++;
				break;
			case 3:
				animacao.Play ("eat");
				n++;
				break;
			}
			if (n == 4)
				n = 0;
		}
	}
}
