﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerAfricanDeer : MonoBehaviour {

	public Animation animacao;
	private int n;
	public AudioClip deerAgressive;
	public AudioClip deerDamage;
	public AudioClip deerIdle;

	// Use this for initialization
	void Start () { 
		n = 0;
		animacao = this.GetComponent<Animation> ();

	}
	
	// Update is called once per frame
	void OnMouseDown(){
		if (Input.GetMouseButtonDown (0)) {
			switch (n) {
			case 0:
				animacao.Play ("attack");
				GetComponent<AudioSource> ().PlayOneShot (deerAgressive,0.7F);
				n++;
				break;
			case 1:
				animacao.Play ("death");
				n++;
				break;
			case 2:
				animacao.Play ("eat");
				n++;
				break;
			case 3:
				animacao.Play ("hurt");
				GetComponent<AudioSource> ().PlayOneShot (deerDamage,0.7F);
				n++;
				break;
			case 4:
				animacao.Play ("idle");
				GetComponent<AudioSource> ().PlayOneShot (deerIdle,0.7F);
				n++;
				break;
			case 5:
				animacao.Play ("jump");
				n++;
				break;
		
			}
			if (n == 6)
				n = 0;
		}
	}
}
