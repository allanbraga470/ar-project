﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerTRex : MonoBehaviour {

	public Animation animacao;
	private int n;
	public AudioClip trexAttack;
	public AudioClip trexHurt;
	public AudioClip trexRoar;

	// Use this for initialization
	void Start () { 
		n = 0;
		animacao = this.GetComponent<Animation> ();

	}

	void Update(){
		if(!animacao.isPlaying)
			animacao.Play ("idle");
	}

	// Update is called once per frame
	void OnMouseDown(){
		if (Input.GetMouseButtonDown (0)) {
			switch (n) {
			case 0:
				animacao.Play ("attack");
				GetComponent<AudioSource> ().PlayOneShot (trexAttack,0.7F);
				n++;
				break;
			case 1:
				animacao.Play ("Roar");
				GetComponent<AudioSource> ().PlayOneShot (trexRoar,0.7F);
				n++;
				break;
			case 2:
				animacao.Play ("hurt");
				GetComponent<AudioSource> ().PlayOneShot (trexHurt,0.7F);
				n++;
				break;
			case 3:
				animacao.Play ("idle");
				n++;
				break;
			case 4:
				animacao.Play ("death");
				n++;
				break;
			case 5:
				animacao.Play ("idle");
				n++;
				break;
			}
			if (n == 6)
				n = 0;
		}
	}
}
