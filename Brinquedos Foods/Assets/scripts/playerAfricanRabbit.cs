﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerAfricanRabbit : MonoBehaviour {

	public Animation animacao;
	private int n;
	public AudioClip deerAgressive;
	public AudioClip deerDamage;
	public AudioClip deerIdle;

	// Use this for initialization
	void Start () { 
		n = 0;
		animacao = this.GetComponent<Animation> ();
	}

	void Update(){
		if(!animacao.isPlaying)
			animacao.Play ("idle");
	}
	
	// Update is called once per frame
	void OnMouseDown(){
		if (Input.GetMouseButtonDown (0)) {
			switch (n) {
			case 0:
				animacao.Play ("hurt");
				n++;
				break;
			case 1:
				animacao.Play ("death");
				n++;
				break;
			case 2:
				animacao.Play ("eat");
				n++;
				break;
			case 3:
				animacao.Play ("idle");
				n++;
				break;
			case 4:
				animacao.Play ("idle2");
				n++;
				break;
		
			}
			if (n == 5)
				n = 0;
		}
	}
}
