﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerDragon : MonoBehaviour {

	public Animation animacao;

	// Use this for initialization
	void Start () { 
		animacao = this.GetComponent<Animation> ();
	}
	
	// Update is called once per frame
	void OnMouseDown(){
		if (Input.GetMouseButtonDown (0)) {
			int n = Random.Range (0, 3);
			switch (n) {
			case 0:
				animacao.Play ("Default Take");
				break;
			case 1:
				animacao.Play ("Fly_New");
				break;
			case 2:
				animacao.Play ("Idel_New");
				break;
			}
		}
	}
}
