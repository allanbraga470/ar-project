﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerJurassic : MonoBehaviour {

	public Animator anim;
	public AudioClip somAttack;
	public AudioClip somHit;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> (); 
	}
		
	void OnMouseDown(){
			int n = Random.Range (0, 4);
			switch (n) {
			case 0:
				anim.Play ("Comp|StandA", -1, 0f);
				break;
			case 1:
				anim.Play ("Comp|JumpAttack", -1, 0f);
				GetComponent<AudioSource> ().PlayOneShot (somAttack,0.7F);
				break;
			case 2:
				anim.Play ("Comp|StandB", -1, 0f);
				break;
			case 3:
				anim.Play ("Comp|StandC", -1, 0f);
				break;
		 
		}
	}
}
