﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerAfricanEagle : MonoBehaviour {

	public Animation animacao;
	private int n;
	public AudioClip eagleAttack;
	public AudioClip eagleDamage;
	public AudioClip eagleFly;
	public AudioClip eagleIdle;

	// Use this for initialization
	void Start () { 
		n = 0;
		animacao = this.GetComponent<Animation> ();

	}

	void Update(){
		if(!animacao.isPlaying)
			animacao.Play ("fly");
	}
	
	// Update is called once per frame
	void OnMouseDown(){
		if (Input.GetMouseButtonDown (0)) {
			switch (n) {
			case 0:
				animacao.Play ("attackfly");
				GetComponent<AudioSource> ().PlayOneShot (eagleAttack,0.7F);
				n++;
				break;
			case 1:
				animacao.Play ("death");
				n++;
				break;
			case 2:
				animacao.Play ("fly");
				GetComponent<AudioSource> ().PlayOneShot (eagleFly,0.7F);
				n++;
				break;
			case 3:
				animacao.Play ("deathfly");
				n++;
				break;
			case 4:
				animacao.Play ("fly");
				GetComponent<AudioSource> ().PlayOneShot (eagleFly,0.7F);
				n++;
				break;
			case 5:
				animacao.Play ("hurtfly");
				GetComponent<AudioSource> ().PlayOneShot (eagleDamage,0.7F);
				n++;
				break;
			case 6:
				animacao.Play ("eats");
				n++;
				break;
			case 7:
				animacao.Play ("idle");
				GetComponent<AudioSource> ().PlayOneShot (eagleIdle,0.7F);
				n++;
				break;
			}
			if (n == 8)
				n = 0;
		}
	}
}
