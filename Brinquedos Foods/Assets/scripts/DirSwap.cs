﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
 

public class DirSwap : MonoBehaviour {

	private VuforiaAbstractBehaviour mQCAR;
	public Texture btnTexture;

	void Start() {
		mQCAR = (VuforiaAbstractBehaviour)FindObjectOfType(typeof(VuforiaAbstractBehaviour));

	}

	void OnGUI()
	{
		if (!btnTexture) {
			Debug.LogError("Please assign a texture on the inspector");
			return;
		}

		if (GUI.Button(new Rect(50,35,70,70),btnTexture))
		{
			CameraDevice.CameraDirection currentDir = CameraDevice.Instance.GetCameraDirection();
			if (currentDir == CameraDevice.CameraDirection.CAMERA_BACK || currentDir == CameraDevice.CameraDirection.CAMERA_DEFAULT)
				RestartCamera (CameraDevice.CameraDirection.CAMERA_FRONT, true);
			else {
				RestartCamera (CameraDevice.CameraDirection.CAMERA_BACK, false);
				CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
			}
		}

		/*if (GUI.Button(new Rect(50,100,200,50), "Mirror OFF"))
		{
			RestartCamera(CameraDevice.Instance.GetCameraDirection(), false);
		}

		if (GUI.Button(new Rect(50, 150, 200, 50), "Mirror ON"))
		{
			var config = VuforiaRenderer.Instance.GetVideoBackgroundConfig ();
			config.reflection = VuforiaRenderer.VideoBackgroundReflection.ON;
			VuforiaRenderer.Instance.SetVideoBackgroundConfig(config);

			RestartCamera(CameraDevice.Instance.GetCameraDirection(), true);
		}*/
	}

	private void RestartCamera(CameraDevice.CameraDirection newDir, bool mirror)
	{
		CameraDevice.Instance.Stop();
		CameraDevice.Instance.Deinit();

		CameraDevice.Instance.Init(newDir);

		// Set mirroring 
		var config = VuforiaRenderer.Instance.GetVideoBackgroundConfig();
		config.reflection = mirror ? VuforiaRenderer.VideoBackgroundReflection.ON : VuforiaRenderer.VideoBackgroundReflection.OFF;
		VuforiaRenderer.Instance.SetVideoBackgroundConfig(config);

		CameraDevice.Instance.Start();
	}
}
